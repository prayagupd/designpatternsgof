package commandPattern;

import commandPattern.command.CommandManager;

/**
 * Created by prayagupd
 * on 4/8/15.
 */
public interface Observer {
  public void update(CommandManager commandManager);
}
