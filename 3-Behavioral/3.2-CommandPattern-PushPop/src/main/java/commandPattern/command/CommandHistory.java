package commandPattern.command;

import java.util.*;
/**
 * @author  prayagupd
 * @date    04-08-2015
 */

public class CommandHistory {
    private Vector historyList;
    private int listLastOperationPointerIndex;
    
    public CommandHistory(){
        historyList = new Vector();
        listLastOperationPointerIndex =-1;
    }
    
    public int addCommand(Command command){
        historyList.addElement(command);
        ++listLastOperationPointerIndex;
        return listLastOperationPointerIndex;
    }
    
    public int undo (){
        if (listLastOperationPointerIndex >= 0){
            Command command = (Command) historyList.elementAt(listLastOperationPointerIndex);
            command.undoExecute();
            listLastOperationPointerIndex--;
        }
        return listLastOperationPointerIndex;
    }
    
    public int redo (){
        if (listLastOperationPointerIndex >= -1){
            //first increase from current action
            listLastOperationPointerIndex++;
            Command command = (Command) historyList.elementAt(listLastOperationPointerIndex);
            command.execute();
        }
        return listLastOperationPointerIndex;
    }
    
    public int listSize(){
        return historyList.size();
    }
    public int listPointer(){
        return listLastOperationPointerIndex;
    }
    
}