package commandPattern.command;

import commandPattern.Lab4;
import commandPattern.Observable;
import commandPattern.Observer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 984493
 * on 4/8/2015.
 */

public class CommandManager implements Observable {
    private List<Observer> observers = new ArrayList<Observer>();

    CommandHistory commandHistory = new CommandHistory();

    public void submit(Command command){
        command.execute();
        commandHistory.addCommand(command);
        notifyObservers();
    }

    public void undo() {
      commandHistory.undo();
      notifyObservers();
    }

    public void redo() {
      commandHistory.redo();
      notifyObservers();
    }

    public void repeat() {
      commandHistory.redo();
      notifyObservers();
    }

    public CommandHistory commandHistory(){
        return commandHistory;
    }

    @Override
    public void addObserver(Observer observer){
      this.observers.add(observer);
    }

    public void notifyObservers() {
      observers.forEach(observer -> observer.update(this));
    }
}
