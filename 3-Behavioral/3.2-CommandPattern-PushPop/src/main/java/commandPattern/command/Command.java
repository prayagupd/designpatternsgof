package commandPattern.command;

/**
 * @author  prayagupd
 * @date    04-07-2015
 */

public interface Command {
    public void execute(); // invoke()
    public void undoExecute();
}