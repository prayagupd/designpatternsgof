package filesystem;

import java.util.List;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public interface Functor {
  public void apply(Component element);
  public List<Component> result();
}
