package filesystem;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class Composite extends Component {

  private List<Component> components = new LinkedList<Component>();

  public Composite(final String name, final Component.TYPE type, final long size,
      final Date dateModified){
    super(name, type, size, dateModified);
  }

  @Override
  public double size() {
    int size = 0;
    for (Component component : components) {
      size += component.size();
    }
    return super.size();
  }


  public double size(TYPE type) {
    int size = 0;
    for (Component component : components) {
      if (type.equals(component.getType())) {
        size += component.size();
      }
    }
    return size;
  }

  public void calculateSize(){

    //Function<Double, Double> function = new Function<Double, Double>() {
    //  double sum = 0;
    //
    //  @Override
    //  public Double apply(Double aDouble) {
    //    return sum +=aDouble;
    //  }
    //};
    //components.stream().map(component-> component.size()).reduce(0l, (accumulator, _i)-> accumulator+_i);
  }

  public void displayHierarchy(){
    super.displayHierarchy();
    components.forEach(component -> {
      component.displayHierarchy();
    });
  }

  public void addComponent(Component component){
    components.add(component);
  }

  public List<Component> search(Functor functor){
    components.forEach(component -> {
        functor.apply(component);
    });
    return functor.result();
  }


  public List<Component> search(final TYPE type){
    List<Component> list = new ArrayList<Component>();
    int[] size = {0};
    components.forEach(component -> {
      if (type==null && (TYPE.DOCX.equals(component.getType()) || TYPE.XLS.equals(component.getType())
          || TYPE.TXT.equals(component.getType())) ) {
        size[0]=size[0]+1;
        list.add(component);
      } else if (type.equals(component.getType())) {
        size[0]=size[0]+1;
        list.add(component);
      }
    });
    return list;
  }

}
