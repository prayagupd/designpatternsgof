package filesystem;

import java.util.Date;
import java.util.List;

/**
 * Design a filesystem(UML) that supports xls,txt,docx and folder. The files/folders have name, size
 and date modified properties.

 * Created by prayagupd
 * on 4/16/15.
 */

public abstract class Component {
  public static enum TYPE {
    FOLDER,
    TXT,
    DOCX,
    XLS
  }

  private String name;
  private TYPE type;
  private String alias;
  private long size;
  private Date dateModified;

  public Component(final String name, final Component.TYPE type, final long size,
      final Date dateModified){
    this.name = name;
    this.type = type;
    this.size = size;
    this.dateModified = dateModified;
  }

  public String getName(){return name;}
  public void setSize(final long size){this.size=size;}

  public void setAlias(final String alias){this.alias=alias;}
  public String getAlias(){return alias;}

  public double size(){return size;}

  public void displayHierarchy(){
    final String response = getDisplayMessage();
    System.out.println(response);
  }

  public String getDisplayMessage() {
    final String separator = "\t";
    StringBuilder response = new StringBuilder(this.name)
        .append(separator)
        .append(this.type)
        .append(separator)
        .append(this.size)
        .append(separator)
        .append(this.dateModified)
        .append(separator)
        .append("parent");
    return response.toString();
  }

  public TYPE getType() {
    return type;
  }

  public Date getDateModified(){return dateModified;}

  @Override public String toString() {
    return getDisplayMessage();
  }

  public List<Component> search(Functor functor){
    functor.apply(this);
    return functor.result();
  }
}
