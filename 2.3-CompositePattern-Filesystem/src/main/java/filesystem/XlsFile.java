package filesystem;

import java.util.Date;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class XlsFile extends File {

  public XlsFile(final String name, final long size, final Date dateModified){
    super(name, TYPE.XLS, size, dateModified);
  }
}
