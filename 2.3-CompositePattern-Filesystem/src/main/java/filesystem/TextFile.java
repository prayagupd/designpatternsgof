package filesystem;

import java.util.Date;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class TextFile extends File {

  public TextFile(final String name, final long size, final Date dateModified){
    super(name, TYPE.TXT, size, dateModified);
  }
}
