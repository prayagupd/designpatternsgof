package filesystem;

import java.util.Date;
import java.util.List;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class App {

  public static void main(String[] args){
    Composite rootFolder = new Folder("root", 100, new Date("12/12/1998"));

    rootFolder.addComponent(
        new TextFile("configuration.txt", 50, new Date("12/12/1998")));
    rootFolder.addComponent(
        new DocxFile("configuration.doc", 20, new Date("12/12/1998")));

    Composite srcFolder = new Folder("src", 30, new Date("12/12/1998"));
    srcFolder.addComponent(new File("App.java", Component.TYPE.XLS, 30, new Date("12/12/1998")));

    rootFolder.addComponent(srcFolder);

    //
    // 	b. Add new file Readme.txt in a new folder cs433 in the folder User.
    // c. displayHierarchy the filestructure as above
    // d. implement search functionality by name.
    //
    System.out.println("=============================================");
    rootFolder.displayHierarchy();

    System.out.println("=============================================");
    System.out.println("Size of rootDrive = " + rootFolder.size());
    System.out.println("=============================================");


    List<Component> searchCompo = rootFolder.search(new SearchFunctor("src"));
    searchCompo.forEach(component -> {
       System.out.println(component.getDisplayMessage());
    });

    System.out.println("=============================================");
    //4. The application should search all the text files in the system.
    List<Component> searchCompoByText =  rootFolder.search(Component.TYPE.TXT);
    // 5. The application should calculate the size of all docx files in the system.
    searchCompoByText.forEach(component -> {
      System.out.println(component.getDisplayMessage());
    });

    System.out.println("=============================================");
    //5. The application should calculate the size of all docx files in the system.
    System.out.println("DOCX files size = " + rootFolder.size(Component.TYPE.DOCX));

    //6. The application should have functionality to search a file by name/alias and show absolute location.
    //7.Application should be able to filter the files/folder with name, size, type and dateofmodification.
  }
}
