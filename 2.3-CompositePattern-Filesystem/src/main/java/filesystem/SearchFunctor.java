package filesystem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class SearchFunctor implements Functor {

  private String element;
  private List<Component> list = new ArrayList<Component>();

  public SearchFunctor(String element){
    this.element = element;
  }

  @Override
  public void apply(Component elem) {
    if (elem.getName().equals(element))
      list.add(elem);

  }

  @Override
  public List<Component> result() {
    return list;
  }
}
