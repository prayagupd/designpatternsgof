package filesystem;

import java.util.Date;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class Folder extends Composite {

  public Folder(String name, final long size, final Date dateModified){
    super(name, TYPE.FOLDER, size, dateModified);
  }
}
