package filesystem;

import java.util.Date;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class File extends Component {

  public File(final String name, final Component.TYPE type, final long size, final Date dateModified){
    super(name, type, size, dateModified);
  }
}
