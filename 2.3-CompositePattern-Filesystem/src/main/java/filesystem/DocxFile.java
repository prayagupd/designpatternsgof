package filesystem;

import java.util.Date;

/**
 * Created by prayagupd
 * on 4/16/15.
 */

public class DocxFile extends File {

  public DocxFile(final String name, final long size, final Date dateModified){
    super(name, TYPE.DOCX, size, dateModified);
  }
}
